use std::collections::HashMap;
use std::fmt;
use std::io::{Read, Result, Write};

#[macro_use]
extern crate failure;

// The main specification for this protocol is https://iterm2.com/documentation-images.html

pub mod dimension;
pub use dimension::Dimension;

#[derive(Clone)]
struct ArgumentMap(HashMap<&'static str, String>);

impl ArgumentMap {
    fn new() -> ArgumentMap {
        ArgumentMap(HashMap::new())
    }

    fn insert(&mut self, key: &'static str, value: impl Into<String>) {
        self.0.insert(key, value.into());
    }

    fn from_action(action: &TransferAction) -> ArgumentMap {
        let mut args = ArgumentMap::new();

        match action {
            TransferAction::Download => {
                args.insert("inline", "0");
            }

            TransferAction::Display {
                height,
                width,
                preserve_aspect_ratio,
            } => {
                if let Some(width) = width {
                    args.insert("width", *width);
                }

                if let Some(height) = height {
                    args.insert("height", *height);
                }

                if let Some(preserve_aspect_ratio) = *preserve_aspect_ratio {
                    args.insert(
                        "preserveAspectRatio",
                        if preserve_aspect_ratio { "1" } else { "0" },
                    );
                }

                args.insert("inline", "1");
            }
        }

        args
    }
}

impl fmt::Display for ArgumentMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0
            .iter()
            .map(|(name, value)| write!(f, "{}={};", name, value))
            .collect()
    }
}

/// Transfer action, the type of action to complete and it's options.
#[derive(Clone, Copy)]
pub enum TransferAction {
    Download,
    Display {
        width: Option<Dimension>,
        height: Option<Dimension>,
        preserve_aspect_ratio: Option<bool>,
    },
}

// TODO: The whole FileTransfer Struct should be turned into a single function.

/// File transfer instance
///
/// Start the transfer it by using the `transfer` method.
pub struct FileTransfer<'a> {
    action: &'a TransferAction,
    data: Box<dyn Read>,
    name: Option<String>,
}

impl<'a> FileTransfer<'a> {
    /// Create a new file transfer
    pub fn new(
        action: &'a TransferAction,
        data: Box<dyn Read>,
        name: Option<String>,
    ) -> FileTransfer {
        FileTransfer { action, data, name }
    }

    /// Start the transfer on the supplied output writer
    pub fn transfer(mut self, output: &mut impl Write) -> Result<()> {
        let mut args: ArgumentMap = ArgumentMap::from_action(self.action);

        // I'm suprised this passed the lifetime checker
        let encoded_name;
        if let Some(name) = self.name {
            encoded_name = base64::encode(&name);
            args.insert("name", &encoded_name);
        }

        let mut contents = Vec::new();
        let size = self.data.read_to_end(&mut contents)?;
        let size_string = size.to_string();
        args.insert("size", &size_string);

        // TODO: Switch to a streaming base64 implementation
        write!(
            output,
            "\x1b]1337;File={}:{}\x07",
            args,
            base64::encode(&contents)
        )
    }
}
